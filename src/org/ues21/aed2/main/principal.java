package org.ues21.aed2.main;

import org.ues21.aed2.modelo.avl.ArbolAVL;

public class principal {

    /**
     * @param args
     * @throws java.lang.Exception
     */
    public static void main(String[] args) throws Exception {

        // Alumno: 	
        // Legajo:
        // DNI: 
        // email: 
        ArbolAVL arbol = new ArbolAVL();
        
        arbol.insertarAVL(10);
        arbol.insertarAVL(6);
        arbol.insertarAVL(4);
        arbol.insertarAVL(12);
        arbol.insertarAVL(11);
        arbol.insertarAVL(13);
        arbol.insertarAVL(14);
        arbol.insertarAVL(15);
        arbol.insertarAVL(16);
        arbol.insertarAVL(17);
        arbol.insertarAVL(2);
        arbol.insertarAVL(3);
        arbol.eliminarAVL(16);
        arbol.eliminarAVL(10);
        arbol.insertarAVL(18);
        arbol.insertarAVL(19);
        arbol.insertarAVL(16);
//        System.out.println("Cant " + arbol.getCantNodos());

//        System.out.println(arbol.buscarRecur(arbol.getRaiz(), 12));

        arbol.mostrarAscendientes(12);

        System.out.print(arbol.cantDescendientes(13));

    }
}
