/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.ues21.aed2.modelo;

/**
 * La clase NodoArbol representa un "nodo" de un árbol.
 * @author agustin
 * @param <T> Indica que es un tipo generico para Java (el atributo info puede
 * ser una instancia de una clase derivada de Object o incluso un tipo
 * nativo)
 */
public class NodoArbol<T> {
    
    /**
     * Los atributos del nodo, para el ejemplo practico el atributo 
     * info es solo un numero pero por lo general suele ser de tipo 
     * Object o un tipo generico <T>.
     */
    private T info;
    private NodoArbol izquierdo;
    private NodoArbol derecho;

    
    public NodoArbol(T info){
        this.info = info;
    }

    public NodoArbol(NodoArbol izquierdo, T info, NodoArbol derecho) {
        this(info);
        this.izquierdo = izquierdo;
        this.derecho = derecho;
    }
    
    /**
     * @return the info
     */
    public T getInfo() {
        return info;
    }

    /**
     * @param info the info to set
     */
    public void setInfo(T info) {
        this.info = info;
    }

    /**
     * @return the izquierdo
     */
    public NodoArbol getIzquierdo() {
        return izquierdo;
    }

    /**
     * @param izquierdo the izquierdo to set
     */
    public void setIzquierdo(NodoArbol izquierdo) {
        this.izquierdo = izquierdo;
    }

    /**
     * @return the derecho
     */
    public NodoArbol getDerecho() {
        return derecho;
    }

    /**
     * @param derecho the derecho to set
     */
    public void setDerecho(NodoArbol derecho) {
        this.derecho = derecho;
    }

    @Override
    public String toString() {
        return info.toString();
    }
    
    
}