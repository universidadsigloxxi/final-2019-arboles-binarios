/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ues21.aed2.modelo;

/**
 *
 * @author agustin
 */
public class Pila {

    Nodo frente;

    /**
     * Método que permite agregar un nodo a la pila
     *
     * @param info
     */
    public void poner(int info) {
        Nodo p = new Nodo(info, this.frente);
        this.frente = p;
    }
    
    public <T> void poner(T info) {
        Nodo p = new Nodo(info, this.frente);
        this.frente = p;
    }

    /**
     * Método que permite quitar un elemento de la pila
     *
     * @return El nodo recien quitado
     */
    public Nodo quitar() {
        Nodo borrado = null;
        if (this.frente != null) {
            Nodo p = this.frente;
            borrado = p;
            this.frente = this.frente.getSiguiente();
            p.setSiguiente(null);
            p = null;
            int i=0;
            i++;
        }
        return borrado;
    }

    public void cargarComunes(Pila p1, Pila p2, Pila p3) {
        Nodo n1 = p1.quitar();
        Nodo n2 = p2.quitar();

        while (n1 != null && n2 != null) {
            if ((int) n1.getInfo() == (int) n2.getInfo()) {
                p3.poner((int) n1.getInfo());
            }
            n1 = p1.quitar();
            n2 = p2.quitar();
        }

        mostrar(p3);
    }

    public void cargarComunesRecursivo(Pila p1, Pila p2, Pila p3) {
        p3 = cargarComunesRec(p1, p2, p3);
        mostrar(p3);
    }

    private Pila cargarComunesRec(Pila p1, Pila p2, Pila p3) {
        Nodo n1 = p1.quitar();
        Nodo n2 = p2.quitar();

        if (n1 != null && n2 != null) {
            if ((int) n1.getInfo() == (int) n2.getInfo()) {
                p3.poner((int) n1.getInfo());
            }
            cargarComunesRec(p1, p2, p3);
        }
        return p3;
    }

    public boolean esCapicua(char[] texto) {
        int n = texto.length;
        int mitad = n / 2;

        //LLenado de pila
        for (int i = 0; i < mitad; i++) {
            poner(texto[i]);
        }

        mitad = (n % 2 != 0) ? mitad + 1 : mitad;
        Nodo x;

        //verifico si es capicua
        for (int i = mitad; i < n; i++) {
            x = quitar();
            if ((int) x.getInfo() != (int) texto[i]) {
                return false;
            }
        }

        return true;//Es capicua
    }

    public void mostrar(Pila p) {
        Nodo n1 = p.quitar();

        while (n1 != null) {
            System.out.println("Nodo: " + n1.getInfo());
            n1 = p.quitar();
        }
    }

    public void mostrarPilaRecursivo() {
        Nodo f = this.frente;
        mostrarPilaRec(f);
    }

    private void mostrarPilaRec(Nodo p) {
        if (p != null) {
            System.out.println("Nodo de pila: " + p.getInfo());
            mostrarPilaRec(p);
        }
    }
    
    public void invertir(Pila p) {
        Nodo n1 = p.quitar();
        Pila pNueva = new Pila();
        while(n1 != null) {
            pNueva.poner((int) n1.getInfo());
            n1 = p.quitar();
        }
        this.frente = pNueva.frente;
    }
    
    public void invertirRecursivo(Pila p) {
        Pila pNueva = invertirRec(p.frente, p);
        this.frente = pNueva.frente;
    }
    
    private Pila invertirRec(Nodo n, Pila p) {
        if(n != null) {
            p.poner((int) n.getInfo());
            invertirRec(n, p);
        }
        return p;
    }

}
