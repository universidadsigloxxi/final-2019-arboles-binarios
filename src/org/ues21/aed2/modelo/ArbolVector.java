/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ues21.aed2.modelo;

import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author agustin
 */
public class ArbolVector {

    String[] arbol = new String[3];

    public void cargarArbol() {
        Scanner scan = new Scanner(System.in);

        System.out.println("Ingrese la raiz del árbol: ");

        String linea = scan.nextLine();

        int i = 0;

        while (true) {
            if (i == 0) {
                arbol[i] = linea;
            }
            System.out.println("Raiz: " + arbol[i]);
            System.out.println("Ingrese el hijo izquierdo: ");
            linea = scan.nextLine();
            //hijo izq
            arbol[2 * i + 1] = linea;

            System.out.println("Ingrese el hijo derecho: ");
            linea = scan.nextLine();
            //hijo der
            arbol[2 * i + 2] = linea;

            System.out.println("Desea agregar más nodos: (si/no)");
            linea = scan.nextLine();
            if (linea.toLowerCase().equals("no")) {
                break;
            }

            arbol = Arrays.copyOf(arbol, arbol.length + 2);

            i++;

        }

    }

    public void mostrarNodosNivel() {
        try {
            Scanner scan = new Scanner(System.in);
            System.out.println("Ingrese el nivel a consultar: ");

            int nivel = scan.nextInt();

            int cantNodosNivel = (int) Math.pow(2, nivel);
            if (cantNodosNivel < arbol.length) {
                int ini = cantNodosNivel - 1;
                int fin = ini + cantNodosNivel;

                System.out.println("Mostrando Nodos en nivel[" + nivel + "]:");
                for (int i = ini; i < fin; i++) {
                    System.out.println("[" + arbol[i] + "]");
                }
            } else {
                System.err.println("Ese nivel no se encuentra en el árbol!!");
            }
        } catch (InputMismatchException e) {
            System.err.println("Ese nivel no se encuentra en el árbol!!");
        }

    }

    public void encontrarHijosIzqDer() {
        Scanner scan = new Scanner(System.in);
        System.out.println("Ingrese el nodo a consultar: ");

        String nodo = scan.nextLine();
        boolean encontro = false;

        for (int i = 0; i < arbol.length; i++) {
            if (arbol[i].equals(nodo)) {
                encontro = true;
                System.out.println("Nodo Encontrado");
                int hi = 2 * i + 1;
                int hd = 2 * i + 2;
                if (hi >= arbol.length) {
                    System.out.println("No tiene hijo izq");
                } else {
                    System.out.println("Hijo izq: " + arbol[hi]);
                }
                if (hd >= arbol.length) {
                    System.out.println("No tiene hijo der");
                } else {
                    System.out.println("Hijo izq: " + arbol[hd]);
                }
                break;
            }
        }
        if (!encontro) {
            System.err.println("Ese nodo no se encuentra!!");
        }

    }

}
