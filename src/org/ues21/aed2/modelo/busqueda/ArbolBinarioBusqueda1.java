/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ues21.aed2.modelo.busqueda;

import org.ues21.aed2.modelo.*;

/**
 *
 * @author agustin
 */
public class ArbolBinarioBusqueda1 {

    protected NodoArbol raiz;
    private int cantNodos;

    public ArbolBinarioBusqueda1() {
        this.raiz = null;
    }

    public ArbolBinarioBusqueda1(NodoArbol raiz) {
        this.raiz = raiz;
    }

    /**
     * @return the raiz
     */
    public NodoArbol getRaiz() {
        return raiz;
    }

    /**
     * @param raiz the raiz to set
     */
    public void setRaiz(NodoArbol raiz) {
        this.raiz = raiz;
    }

    public boolean esVacio() {
        return (this.raiz == null);
    }

    public int obtenerAltura(NodoArbol nodo) {
        int alturaParcial, alturaD = 0, alturaI = 0;
        NodoArbol p = nodo;
        if (p.getIzquierdo() != null) {
            alturaI = obtenerAltura(p.getIzquierdo());
        }
        if (p.getDerecho() != null) {
            alturaD = obtenerAltura(p.getDerecho());
        }
        if (alturaI > alturaD) {
            alturaParcial = alturaI + 1;
        } else {
            alturaParcial = alturaD + 1;
        }
        return alturaParcial;
    }

    public int obtenerAltura() {
        if (this.raiz == null) {
            return 0;
        } else {
            return obtenerAltura(this.raiz);
        }
    }

    /**
     * Método que permite insertar en el árbol binario de búsqueda de manera que
     * los valores menores van a izquierda y los valores mayores van a derecha.
     *
     * @param nvoNro La nueva información que se desea insertar en el árbol de
     * busqueda de manera ordenada
     */
    public void insertar(int nvoNro) {
        NodoArbol p = this.raiz, q = null;

        while (p != null) {
            if (nvoNro == ((int) p.getInfo())) {
                break;
            }

            //q sigue a p para poder insertar el nodo hoja en el lugar correcto
            q = p;

            if (nvoNro < ((int) p.getInfo())) {
                p = p.getIzquierdo();
            } else {
                p = p.getDerecho();
            }

        }

        if (p == null) {
            //No estaba entonces lo puedo insertar
            NodoArbol nuevo = new NodoArbol(null, nvoNro, null);
            if (q == null) {
                this.raiz = nuevo;
            } else if (nvoNro < ((int) q.getInfo())) {
                q.setIzquierdo(nuevo);
            } else {
                q.setDerecho(nuevo);
            }
        }
    }

    /**
     * Método recursivo que permite insertar en el árbol binario de búsqueda de
     * manera que los valores menores van a izquierda y los valores mayores van
     * a derecha.
     *
     * @param p El puntero al nodo inicial en donde comienza el recorrido (La
     * raiz).
     * @param q El puntero que sigue a p para saber en que posición debo
     * insertar el nuevo nodo.
     * @param nvoNro La nueva información que se desea insertar en el árbol de
     * busqueda de manera ordenada
     */
    public void insertarRecur(NodoArbol p, NodoArbol q, int nvoNro) {
        if (p == null) {
            NodoArbol nvo = new NodoArbol(null, nvoNro, null);
            if (q == null) {
                this.raiz = nvo;
            } else if (nvoNro < ((int) q.getInfo())) {
                q.setIzquierdo(nvo);
            } else {
                q.setDerecho(nvo);
            }
        } else {
            q = p;
            insertarRecur(nvoNro < ((int) p.getInfo()) ? p.getIzquierdo() : p.getDerecho(), q, nvoNro);
        }
    }

    /**
     * Método que permite buscar un nodo en un árbol binario de busqueda La idea
     * es que lo puedan realizar los alumnos en clase.
     *
     * @param nroBuscado El numero que se esta intentando encontrar
     * @return Devuelve el nodo encontrado cuya info concuerda con el nro
     * buscado.
     */
    public NodoArbol buscar(int nroBuscado) {

        NodoArbol p = this.raiz;

        while (p != null) {
            if (((int) p.getInfo()) == nroBuscado) {
                break;
            }

            if (nroBuscado < ((int) p.getInfo())) {
                p = p.getIzquierdo();
            } else {
                p = p.getDerecho();
            }

        }

        return p;
    }

    /**
     * Método de búsqueda recursivo de un nodo en el árbol. La idea es que los
     * alumnos puedan desarrollarlo en clases
     *
     * @param p El puntero al nodo inicial en donde comienza el recorrido (La
     * raiz).
     * @param nroBuscado El numero que se esta intentando encontrar
     * @return Devuelve el nodo encontrado cuya info concuerda con el nro
     * buscado.
     */
    public NodoArbol buscarRecur(NodoArbol p, int nroBuscado) {
        if (p != null) {
            if (nroBuscado == ((int) p.getInfo())) {
                return p;
            }

            if (nroBuscado < ((int) p.getInfo())) {
                p = buscarRecur(p.getIzquierdo(), nroBuscado);
            } else {
                p = buscarRecur(p.getDerecho(), nroBuscado);
            }
        }

        return p;
    }

    public void eliminar(int x) {
        this.eliminar(this.raiz, x);
    }
    
    /**
     * Método que permite eliminar un nodo del árbol independientemente si posee
     * 0, 1 o 2 hijos
     *
     * @param p El puntero al nodo inicial en donde comienza el recorrido (La
     * raiz).
     * @param x La info que contiene el nodo que desea eliminarse.
     * @return Devuelve el nodo que ha sido eliminado
     */
    public NodoArbol eliminar(NodoArbol p, int x) {
        if (p != null) {
            if (x < (int) p.getInfo()) {
                p.setIzquierdo(eliminar(p.getIzquierdo(), x));
            } else if (x > (int) p.getInfo()) {
                p.setDerecho(eliminar(p.getDerecho(), x));
            } else //Lo encontre debo borrar el nodo
            //verifico s tiene 1 hijo (o ninguno)
             if (p.getIzquierdo() == null) {
                    p = p.getDerecho();
                } else if (p.getDerecho() == null) {
                    p = p.getIzquierdo();
                } else {
                    //Tiene dos hijos
                    p.setIzquierdo(dosHijos(p.getIzquierdo(), p));
                }
        }
        return p;
    }

    /**
     * Método de soporte para cuando el nodo que desea ser eliminado posee dos
     * hijos
     *
     * @param d El puntero siempre se deve mover a derecha si el primer
     * movimiento fue a izquierda.
     * @param p El nodo que quiero eliminar y que intercambiara su valor de info
     * con el nodo mayor de sus menores.
     * @return El nodo hijo "mayor de los nemores" del nodo p
     */
    protected NodoArbol dosHijos(NodoArbol d, NodoArbol p) {
        if (d.getDerecho() != null) {
            d.setDerecho(dosHijos(d.getDerecho(), p));
        } else {
            p.setInfo(d.getInfo());
            d = d.getIzquierdo();
        }
        return d;
    }

    /**
     * Método para recorrer un arbol binario de busqueda en preorden u orden
     * previo.
     *
     * @param p El puntero al nodo inicial en donde comienza el recorrido (La
     * raiz).
     */
    public void imprimirPreorden(NodoArbol p) {
        if (p != null) {
            //Visito el nodo
            System.out.println(p.getInfo());
            //Visito el nodo
            imprimirPreorden(p.getIzquierdo());
            imprimirPreorden(p.getDerecho());
        }
    }

    /**
     * Método para recorrer un arbol binario de busqueda en entreorden u orden
     * central.
     *
     * @param p El puntero al nodo inicial en donde comienza el recorrido (La
     * raiz).
     */
    public void imprimirEntreorden(NodoArbol p) {
        if (p != null) {
            imprimirEntreorden(p.getIzquierdo());
            //Visito el nodo
            System.out.println(p.getInfo());
            //Visito el nodo
            imprimirEntreorden(p.getDerecho());
        }
    }

    /**
     * Método para recorrer un arbol binario de busqueda en entreorden u orden
     * posterior.
     *
     * @param p El puntero al nodo inicial en donde comienza el recorrido (La
     * raiz).
     */
    public void imprimirPostorden(NodoArbol p) {
        if (p != null) {
            imprimirPostorden(p.getIzquierdo());
            imprimirPostorden(p.getDerecho());
            //Visito el nodo
            System.out.println(p.getInfo());
            //Visito el nodo
        }
    }

    /**
     * Método que devuelve la cantidad de nodos de un árbol
     *
     * @return La cantidad de nodos del árbol
     */
    public int getCantNodos() {
        this.cantNodos = 0;
        calcularCantNodos(this.raiz);
        return this.cantNodos;
    }

    /**
     * Método auxiliar recursivo que ayuda a calcular la cantidad de nodos del
     * árbol
     *
     * @param p El puntero al nodo actual
     */
    private void calcularCantNodos(NodoArbol p) {
        if (p != null) {
            this.cantNodos++;
            calcularCantNodos(p.getIzquierdo());
            calcularCantNodos(p.getDerecho());
        }
    }

    /**
     * Método que devuelve la cantidad de nodos de un nivel deseado.
     *
     * @param nivelBuscado El nivel del que se desea saber la cantidad de nodos.
     * @return La cantidad de nodos que hay en el nivel deseado
     */
    public int getCantidadNodosEnNivel(int nivelBuscado) {
        return calcularCantidadNodosEnNivel(raiz, nivelBuscado, 0);
    }

    /**
     * Método auxiliar recursivo que ayuda a calcular la cantidad de nodos de un
     * nivel deseado.
     *
     * @param p El puntero al nodo actual
     * @param nivelBuscado El nivel deseado
     * @param nivelActual El nivel actual
     * @return La cantidad de nodos que hay en nivelBuscado
     */
    private int calcularCantidadNodosEnNivel(NodoArbol p, int nivelBuscado, int nivelActual) {
        int cont = 0;
        if (p != null) {
            if (nivelActual == nivelBuscado) {
                return 1;
            } else {
                cont = cont + calcularCantidadNodosEnNivel(p.getIzquierdo(), nivelBuscado, nivelActual + 1);
                cont = cont + calcularCantidadNodosEnNivel(p.getDerecho(), nivelBuscado, nivelActual + 1);
            }
        }
        return cont;
    }

    /**
     * Método que devuelve la cantidad de nodos de un determinado nivel Muchas
     * gracias a los alumnos Emiliano Cenizo y Sara Iglesias Gracias por la
     * participación a los alumnos Emiliano Cenizo, Sara Iglesias
     *
     * @author Emiliano Cenizo, Sara Iglesias
     * @param nivel El nivel deseado
     * @return La cantidad de nodos
     */
    public int nodosEnNivelN(int nivel) {
        int nivelActual = 0;
        NodoArbol p = this.raiz;
        if (p == null) {
            return 0;
        }

        return this.nodosEnNivelN(p, nivel, nivelActual);
    }

    /**
     * Método auxiliar recursivo que ayuda a calcular la cantidad de nodos en un
     * nivel dado. Gracias por la participación a los alumnos Emiliano Cenizo,
     * Sara Iglesias
     *
     * @author Emiliano Cenizo, Sara Iglesias
     * @param p El puntero al nodo actual
     * @param nivelTarget El nivel búscado
     * @param nivelActual El nivel actual que se esta recorriendo
     * @return La cantidad de nodos en un determinado nivel
     */
    private int nodosEnNivelN(NodoArbol p, int nivelTarget, int nivelActual) {

        int nivelFinal = 0;

        if (nivelActual == nivelTarget) {
            return 1;
        }

        if (p.getIzquierdo() != null) {
            if (nivelActual != nivelTarget) {
                nivelFinal = nivelFinal
                        + this.nodosEnNivelN(p.getIzquierdo(), nivelTarget,
                                nivelActual + 1);
            }
        }
        if (p.getDerecho() != null) {
            if (nivelActual != nivelTarget) {
                nivelFinal = nivelFinal
                        + this.nodosEnNivelN(p.getDerecho(), nivelTarget,
                                nivelActual + 1);
            }
        }

        return nivelFinal;
    }

    public int contadorNodosHoja() {
        NodoArbol p = this.getRaiz();
        return cantNodosHojas(p);
    }
    
    private int cantNodosHojas(NodoArbol p) {
        int cont = 0;
        if (p != null) {
            if (p.getIzquierdo() == null && p.getDerecho() == null) {
                return 1;
            }
            cont = cantNodosHojas(p.getIzquierdo());
            cont = cantNodosHojas(p.getDerecho());
        }
        return cont;
    }
    
//Version correcta    
//    private int cantNodosHojas(NodoArbol p) {
//        int cont = 0;
//        if (p != null) {
//            if (p.getIzquierdo() == null && p.getDerecho() == null) {
//                return 1;
//            }
//            cont = cont + cantNodosHojas(p.getIzquierdo());
//            cont = cont + cantNodosHojas(p.getDerecho());
//        }
//        return cont;
//    }
    
    

}
