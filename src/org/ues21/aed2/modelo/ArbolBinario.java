/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ues21.aed2.modelo;

/**
 *
 * @author agustin
 */
public class ArbolBinario {

    private NodoArbol raiz;

    public ArbolBinario(NodoArbol raiz) {
        this.raiz = raiz;
    }

    /**
     *
     * @param <T>
     * @param izq
     * @param info
     * @param der
     * @return
     */
    public static <T> NodoArbol nuevoSubArbol(NodoArbol izq, T info, NodoArbol der) {
        return new NodoArbol(izq, info, der);
    }

    /**
     * @return the raiz
     */
    public NodoArbol getRaiz() {
        return raiz;
    }

    /**
     * @param raiz the raiz to set
     */
    public void setRaiz(NodoArbol raiz) {
        this.raiz = raiz;
    }
    
    public boolean isVacio() {
        return (this.raiz == null);
    }

    public void mostrar() {
        NodoArbol p = this.raiz;
        mostrarRec(p);
    }

    private void mostrarRec(NodoArbol n) {
        if (n != null) {
            System.out.println("Nodo: " + n.getInfo());
            if (n.getIzquierdo()!= null) {
                mostrarRec(n.getIzquierdo());
            }
            if (n.getDerecho()!= null) {
                mostrarRec(n.getDerecho());
            }
        }
    }
    
    /**
     * Mostrando el arbol de manera recursiva sin ningún tipo de indicación de
     * quien es hijo de quien.
     *
     * @param p Comienza siendo la raiz del árbol
     */
    public void mostrarArbol(NodoArbol p) {
        if (p != null) {
            System.out.println("Nodo: " + p.getInfo());
            if (p.getIzquierdo() != null) {
                mostrarArbol(p.getIzquierdo());
            }
            if (p.getDerecho() != null) {
                mostrarArbol(p.getDerecho());
            }
        }
    }

    /**
     * Mostrando el arbol de manera recursiva indicando quien es hijo de quien.
     * La idea es que lo puedan realizar los alumnos en clase.
     *
     * @param p Comienza siendo la raiz del árbol
     */
    public void mostrarArbolIndicandoHijos(NodoArbol p) {
        if (p != null) {
            if (p.getIzquierdo() != null) {
                System.out.println("Nodo: " + p.getInfo() + " --> Hijo Izq: " + p.getIzquierdo().getInfo());
                mostrarArbolIndicandoHijos(p.getIzquierdo());
            }

            if (p.getDerecho() != null) {
                System.out.println("Nodo: " + p.getInfo() + " --> Hijo Der: " + p.getDerecho().getInfo());
                mostrarArbolIndicandoHijos(p.getDerecho());
            }
        }
    }

    /**
     * Mostrando el arbol de manera recursiva indicando quien es hijo de quien.
     * Muchas gracias a los alumnos Walter Acosta y Alejandro Bonaudi 
     * @author Walter Acosta, Alejandro Bonaudi 
     * @param p Comienza siendo la raiz del árbol
     */
    public void mostrarArbolConHijos(NodoArbol p) {
        if (p != null) {
            System.out.println("-----------------------\nNodo: " + p.getInfo());
            if (p.getIzquierdo() != null) {
                System.out.println("Nodo Izquierdo de " + p.getInfo() + ": " + p.getIzquierdo().getInfo());
                mostrarArbol(p.getIzquierdo());
            } else {
                System.out.println("el nodo " + p.getInfo() + " no tiene hijo izquierdo. ");
            }
            if (p.getDerecho() != null) {
                System.out.println("Nodo Derecho de " + p.getInfo() + ": " + p.getDerecho().getInfo());
                mostrarArbol(p.getDerecho());
            } else {
                System.out.println("el nodo " + p.getInfo() + " no tiene hijo derecho. \n");
            }
        }
    }

}
