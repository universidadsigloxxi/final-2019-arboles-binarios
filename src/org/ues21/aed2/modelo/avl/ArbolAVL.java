/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ues21.aed2.modelo.avl;

import org.ues21.aed2.modelo.busqueda.ArbolBinarioBusqueda;
import org.ues21.aed2.soporte.Booleano;

/**
 * Esta clase es entregada sin garantia. El profe no se hace responsable por
 * granizo, ni daño contra terceros o reprobación de TP.
 *
 * @author agustin
 */
public class ArbolAVL extends ArbolBinarioBusqueda {

    public ArbolAVL() {
        this.raiz = null;
    }

    public ArbolAVL(NodoAVL raiz) {
        this.raiz = raiz;
    }

    public void insertarAVL(int x) throws Exception {
        Booleano h = new Booleano(false);
        raiz = insertarAvl((NodoAVL) raiz, x, h);
    }

    private NodoAVL insertarAvl(NodoAVL p, int x, Booleano h) {
        NodoAVL p1;
        if (p == null) {
            p = new NodoAVL(x);
            h.setBooleano(true);
        } else if (x < (int) p.getInfo()) {
            NodoAVL iz;
            iz = insertarAvl((NodoAVL) p.getIzquierdo(), x, h);
            p.setIzquierdo(iz);
            //vuelvo por los nodos del camino de búsqueda
            if (h.getBooleano()) {
                //decrementa el factor de equilibrio por aumentar la altura de rama izquierda
                switch (p.getEqui()) {
                    case 1:
                        p.setEqui(0);
                        h.setBooleano(false);
                        break;
                    case 0:
                        p.setEqui(-1);
                        break;
                    case -1:
                        //rotación izquierda
                        p1 = (NodoAVL) p.getIzquierdo();
                        if (p1.getEqui() == -1) {
                            p = rotacionIzqSimple(p, p1);
                        } else {
                            p = rotacionIzqDoble(p, p1);
                        }
                        h.setBooleano(false);
                }
            }
        } else if (x > (int) p.getInfo()) {
            NodoAVL dr;
            dr = insertarAvl((NodoAVL) p.getDerecho(), x, h);
            p.setDerecho(dr);
            //vuelta por los nodos del camino de búsqueda
            if (h.getBooleano()) {
                //incrementa el factor de equilibrio por aumentar la altura de rama derecha
                switch (p.getEqui()) {

                    case 1:
                        //rotación derecha
                        p1 = (NodoAVL) p.getDerecho();
                        if (p1.getEqui() == 1) {
                            p = rotacionDerSimple(p, p1);
                        } else {
                            p = rotacionDerDoble(p, p1);
                        }
                        h.setBooleano(false);
                        break;
                    case 0:
                        p.setEqui(1);
                        break;
                    case -1:
                        p.setEqui(0);
                        h.setBooleano(false);
                }
            }
        } else {
            System.out.println("Claves repetida");
        }

        return p;
    }

    public void eliminarAVL(int x) {
        Booleano h = new Booleano(false);
        raiz = eliminarAvl((NodoAVL) raiz, x, h);
    }

    private NodoAVL eliminarAvl(NodoAVL p, int x, Booleano h) {
        NodoAVL p1;
        if (p != null) {
            if (x < (int) p.getInfo()) {
                p.setIzquierdo(eliminarAvl((NodoAVL) p.getIzquierdo(), x, h));
                //vuelvo por los nodos del camino de búsqueda
                if (h.getBooleano()) {
                    //actualizo factor de equilibrio por variación de la altura
                    switch (p.getEqui()) {
                        case 1:
                            //rotación derecha
                            p1 = (NodoAVL) p.getDerecho();
                            if (p1.getEqui() == 1) {
                                p = rotacionDerSimple(p, p1);
                            } else {
                                p = rotacionDerDoble(p, p1);
                            }
                            h.setBooleano(false);
                            break;
                        case 0:
                            p.setEqui(1);
                            break;
                        case -1:
                            p.setEqui(0);
                            h.setBooleano(false);
                    }
                }
            } else if (x > (int) p.getInfo()) {
                p.setDerecho(eliminarAvl((NodoAVL) p.getDerecho(), x,h));
                //vuelta por los nodos del camino de búsqueda
                if (h.getBooleano()) {
                    //actualizo factor de equilibrio por variación de la altura
                    switch (p.getEqui()) {
                        case 1:
                            p.setEqui(0);
                            h.setBooleano(false);
                            break;
                        case 0:
                            p.setEqui(-1);
                            break;
                        case -1:
                            //rotación izquierda
                            p1 = (NodoAVL) p.getIzquierdo();
                            if (p1.getEqui() == -1) {
                                p = rotacionIzqSimple(p, p1);
                            } else {
                                p = rotacionIzqDoble(p, p1);
                            }
                            h.setBooleano(false);
                    }
                }
            } else {
                //Lo encontre debo borrar el nodo
                //verifico si tiene 1 hijo (o ninguno)
                if (p.getIzquierdo() == null) {
                    p = (NodoAVL) p.getDerecho();
                } else if (p.getDerecho() == null) {
                    p = (NodoAVL) p.getIzquierdo();
                } else {
                    //Tiene dos hijos
                    p.setIzquierdo(dosHijos(p.getIzquierdo(), p));
                }
                h.setBooleano(true);

            }
        }
        return p;
    }

    /**
     * rotacion Izquierda Simple o rotación izquierda izquierda
     *
     * @param p
     * @param p1
     * @return
     */
    private NodoAVL rotacionIzqSimple(NodoAVL p, NodoAVL p1) {
        p.setIzquierdo(p1.getDerecho());
        p1.setDerecho(p);
        p.setEqui(0);
        p1.setEqui(0);
        return p1;
    }

    /**
     * rotacion derecha simple o rotación derecha derecha
     *
     * @param p
     * @param p1
     * @return
     */
    private NodoAVL rotacionDerSimple(NodoAVL p, NodoAVL p1) {
        p.setDerecho(p1.getIzquierdo());
        p1.setIzquierdo(p);
        p.setEqui(0);
        p1.setEqui(0);
        return p1;
    }

    /**
     * rotación izquierda doble o rotación izquierda derecha
     *
     * @param p
     * @param p1
     * @return
     */
    private NodoAVL rotacionIzqDoble(NodoAVL p, NodoAVL p1) {
        NodoAVL p2;

        p2 = (NodoAVL) p1.getDerecho();
        p.setIzquierdo(p2.getDerecho());
        p2.setDerecho(p);
        p1.setDerecho(p2.getIzquierdo());
        p2.setIzquierdo(p1);
        p2.setEqui(0);
        p1.setEqui(0);
        p.setEqui(0);
        return p2;
    }

    /**
     * rotación derecha doble o rotación derecha izquierda
     *
     * @param p
     * @param p1
     * @return
     */
    private NodoAVL rotacionDerDoble(NodoAVL p, NodoAVL p1) {
        NodoAVL p2;
        p2 = (NodoAVL) p1.getIzquierdo();

        p.setDerecho(p2.getIzquierdo());
        p2.setIzquierdo(p);
        p1.setIzquierdo(p2.getDerecho());
        p2.setDerecho(p1);
        p2.setEqui(0);
        p1.setEqui(0);
        p.setEqui(0);
        return p2;
    }

}
