/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ues21.aed2.soporte;

/**
 * Esta clase existe para poder transmitir el valor boolean entre llamadas al método recursivo insertarAvl.
 * Si no se desea usar esta clase h debe ser un atributo de la clase.
 * @author agustin
 */
public class Booleano {

    boolean v;

    public Booleano(boolean valor) {
        v = valor;
    }

    public void setBooleano(boolean valor) {
        v = valor;
    }

    public boolean getBooleano() {
        return v;
    }
}
